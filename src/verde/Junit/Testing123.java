import org.junit.Test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;


public class Testing123 {

   String str1;
   String str2;
   String str3;
   String str4;
   String str5;
   int val1;
   int val2;
   String[] expectedArray = { "one", "two", "three" };
   String[] resultArray = { "one", "two", "three" };

   @Before
   public void executeBeforeEachTest() {

       str1 = new String("abc");
       str2 = new String("abc");
       str3 = null;
       str4 = "abc";
       str5 = "abc";
       val1 = 1;
       val2 = 2;
   }

   @After
   public void testAfterAssertions() {

       str1 = null;
       str2 = null;
       str3 = null;
       str4 = null;
       str5 = null;
       val1 = 0;
       val2 = 0;
   }

   @Test
   public void testEquals() {

       assertEquals(str1, str2);
       assertArrayEquals(expectedArray, resultArray);
   }

   @Test
   public void testSameObject() {

       assertSame(str4, str5);
       assertNotSame(str1, str3);
   }

   @Test
   public void testTrueFalse() {

       assertTrue(val1 < val2);
       assertFalse(val1 > val2);
   }

   @Test
   public void testNullOrNotNull() {

       assertNotNull(str1);
       assertNull(str3);
   }

}

import java.util.*;



public class Collection {



    public static void main(String[] args) {


	System.out.println("This is an example of exception Handling.");
	try{
      		int data=25/0;
   	}catch(ArithmeticException e){System.out.println(e);}
  	System.out.println("You can't divide by zero it doesn't work.");
  	


        System.out.println("This is a list using List.");
	System.out.println("This displays in the order it is given.");
	System.out.println("List of Top Ten Fantasy Football Points");

        List list = new ArrayList();

        list.add("1. Cooper Kupp");

        list.add("2. Josh Allen");

        list.add("3. Justin Herbert");

        list.add("4. Tom Brady");

        list.add("5. Jonathan Taylor");

        list.add("6. Patrick Mahomes");

        list.add("7. Davante Adams");

        list.add("8. Austin Ekeler");

        list.add("9. Deebo Samuel");

        list.add("10. Aaron Rodgers");



        for (Object str : list) {

            System.out.println((String)str);

        }



        System.out.println("This is a list using Set.");
	System.out.println("Sets cannot have a duplicate record making it perfect for a grocery list.");
	System.out.println("Milk and Cereal are listed twice but only display once.");
	System.out.println("My Grocery List");

        Set set = new TreeSet();

        set.add("Milk");

        set.add("Cereal");

        set.add("Hot Cocoa");

        set.add("Toilet Paper");

        set.add("Toy for Duke");

        set.add("Toy for Ricky");

        set.add("Milk");

        set.add("Cereal");



        for (Object str : set) {

            System.out.println((String)str);

        }



        System.out.println("This is a list using Queue.");
	System.out.println("This displays in the order it is given. Like a Queue.");
	System.out.println("List of my Favorite Video Games in Descending Order.");

        Queue queue = new PriorityQueue();

        queue.add("7. Halo Reach");

        queue.add("6. Halo 5");

        queue.add("5. Halo 4");

        queue.add("4. Halo 3");

        queue.add("3. Halo 2");

        queue.add("2. Halo CE");

        queue.add("1. Halo Infinite");



        Iterator iterator = queue.iterator();

        while (iterator.hasNext()) {

            System.out.println(queue.poll());

        }



        System.out.println("This is a list using Map.");

        Map map = new HashMap();

        map.put(1,"Michael Jordan");

        map.put(2,"Kobe Bryant");

        map.put(3,"Magic Johnson");

        map.put(4,"Kareem");

        map.put(5,"Shaq");

        map.put(1,"Lebron James");

        map.put(2,"Michael Jordan");

        map.put(3,"Kobe Bryant");

        map.put(4,"Magic Johnson");

        map.put(5,"Kareem");

        map.put(6,"Shaq");



        for (int i = 1; i < 7; i++) {

            String result = (String)map.get(i);

            System.out.println(result);

        }

Deque<String> deque
            = new LinkedList<String>();
  
 
        deque.add("Josh Allen");
  
        deque.addFirst("Cooper Kupp");
  
        deque.addLast("Jonathan Taylor");
  
        deque.push("Mark Andrews");
  
        deque.offer("This will dissapear");
  
        deque.offerFirst("This will dissapear");
  
        System.out.println(deque + "\n");
  
        deque.removeFirst();
        deque.removeLast();
        System.out.println("This is an example of dequeue "
                           + "Fantasy Football Dream Team: "
                           + deque);

    }

}
package verde.javacollections;



public class Players {

            System.out.println("This is a list using Generics");
	System.out.println("NBA All-Time Leading Points");

        List<players> myList = new LinkedList<players>();

        myList.add(new players("1. Kareem Abdul-Jabbar", "38,387"));

        myList.add(new players("2. Karl Malone", "36,928"));

        myList.add(new players("3. Lebron James", "36,239"));

        myList.add(new players("4. Kobe Bryant", "33,643"));

        myList.add(new players("5. Michael Jordan", "32,292"));



        for (players player : myList) {

            System.out.println(player);

        }

    private String player;

    private String points;



    public players(String player, String points) {

        this.player = player;

        this.points = points;

    }



    public String toString() {

        return "Player: " + player + " Points: " + points;

    }

}
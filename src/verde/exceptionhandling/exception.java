import java.util.Scanner;

public class Exception {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                int n1, n2;
                while (true) {
                        System.out.println("Please enter two numbers and I will divide the first by the second number you give me.");
                        n1 = sc.nextInt();
                        n2 = sc.nextInt();
                        if (n2 != 0)
                                break;
                        else
                                System.out.println("Can't Divide by Zero please enter any number other than zero for the second number.");
                                System.out.println("The first number you entered : " + n1 + " " + "The second number you entered : " + n2);
                } 
                int s = div(n1, n2);
                System.out.println("The result is: " + s);
                System.out.println("The first number you entered : " + n1 + " " + "The second number you entered : " + n2);
        }

        public static int div(int n1, int n2) throws ArithmeticException {
                return n1 / n2;
        }
}
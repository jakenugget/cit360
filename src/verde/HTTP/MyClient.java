import com.fasterxml.jackson.databind.ObjectMapper;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;

public class MyClient {
    public static String executeHttpRequest(String verb, String endpoint, String payload) throws Exception {
        if (endpoint == null || endpoint.isEmpty()) {
            throw new IllegalArgumentException("Url_endpoint_missing!");
        }

        URL url = new URL(endpoint);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod(verb);
        conn.setRequestProperty("Content-Type", "application/json");

        OutputStream os = conn.getOutputStream();
        os.write(payload.getBytes());
        os.flush();

        int code = conn.getResponseCode();
        if (code != HttpURLConnection.HTTP_OK && code != HttpURLConnection.HTTP_CREATED) {
        
            throw new Exception("Something went wrong! \n HTTP error code : " + code);
        }

        InputStreamReader inputStreamReader = new InputStreamReader((conn.getInputStream()));
        BufferedReader br = new BufferedReader(inputStreamReader);
        return br.lines().collect(Collectors.joining());
    }

    public static List<Player> getPlayersByType(String type)  throws Exception {
        String response = executeHttpRequest(
                "GET",
                "http://localhost:9000/players",
                String.format("{\"type\": \"%s\"}", type)
        );

        ObjectMapper objectMapper = new ObjectMapper();
        Player[] players = objectMapper.readValue(response, Player[].class);
        return Arrays.asList(players);
    }

    public static List<Player> getAllPlayers()   throws Exception {
        String response = executeHttpRequest("GET", "http://localhost:9000/players", "");

        
        ObjectMapper objectMapper = new ObjectMapper();
        Player[] players = objectMapper.readValue(response, Player[].class);
        return Arrays.asList(players);
    }

    public static void main(String[] args) throws Exception {
        
        List<Player> players = getAllPlayers();
        for (Player player : players) {
            System.out.println(player);
        }

        
        List<Player> PlayersToo = getPlayersByType("too");
        System.out.println("players too: " + playersToo.size());


        List<Player> basketballs = getPlayersByType("basketball");
        for (Player basketball : basketballs) {
            System.out.println(basketball);
            System.out.println("Swish");
        }


        List<Player> footballs = getPlayersByType("football");
        for (Player football : footballs) {
            System.out.println(football);
            System.out.println("Touchdown");
        }
    }
}

import com.sun.net.httpserver.HttpServer;
import java.io.IOException;
import java.net.InetSocketAddress;

public class MyHttpServer {
    public static void main(String[] args) throws IOException {
        final int PORT = 9000;

        HttpServer httpServer = HttpServer.create(new InetSocketAddress(PORT), 0);
        httpServer.createContext("/players", new PlayerRequestHandler());
        System.out.println("Server_started! \nPORT: " + PORT);
        httpServer.setExecutor(null);
        httpServer.start();
    }
}
public class PlayerRequestPayload {
    private String type;

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
}
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PlayerRequestHandler implements HttpHandler {

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final List<Player> players = new ArrayList<>();
    public PlayerRequestHandler() {

        Player lebron = new Player("Lebron", "basketball");
        Player jordan = new Player("Jordan", "basketball");
        Player patrick = new Player("Patrick", "football");
        Player tom = new Player("Tom", "football");



        players.add(lebron);
        players.add(jordan);
        players.add(patrick);
        players.add(tom);
    }

    @Override
    public void handle(HttpExchange httpExchange)throwsIOException {
        InputStreamReader inputStreamReader = new InputStreamReader(
                httpExchange.getRequestBody(),
                StandardCharsets.UTF_8
        );
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        
        String jsonPayload =  bufferedReader.lines().collect(Collectors.joining());

        if (jsonPayload.isEmpty()) {
            jsonPayload = objectMapper.writeValueAsString(players);
        } else {
            try {
                
                PlayerRequestPayload requestPayload = objectMapper.readValue(jsonPayload, PlayerRequestPayload.class);
                String playerType = requestPayload.getType();

                
                List<Player> players = new ArrayList<>();
                for (Player player : this.players) {
                    if (player.getType().equals(playerType)) {
                        players.add(player);
                    }
                }

                jsonPayload = objectMapper.writeValueAsString(players);
            } catch (IOException e) {
                httpExchange.sendResponseHeaders(400, 0);
                writeHttpResponse(httpExchange.getResponseBody(), "Bad_Request");
                return;
            }
        }

        Headers headers = httpExchange.getResponseHeaders();
        headers.add("Content-Type", "application/json; charset=UTF-8");

       
        httpExchange.sendResponseHeaders(200, jsonPayload.length());
        writeHttpResponse(httpExchange.getResponseBody(), jsonPayload);
    }

     public void writeHttpResponse(OutputStream outputStream, String message) throws IOException {
        outputStream.write(message.getBytes());
        outputStream.close();
    }
}